using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResultPanel : MonoBehaviour
{
    [SerializeField] TMPro.TextMeshProUGUI resultText;

    public void DisplayWithResultText(string text)
    {
        resultText.text = text;

        GetComponent<CanvasGroup>().alpha = 1.0f;
        GetComponent<CanvasGroup>().interactable = true;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene("GameplayScene");
    }
}
