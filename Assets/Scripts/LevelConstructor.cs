using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelConstructor : MonoBehaviour
{
    [SerializeField] private GameObject cubeSpawnerPrefab;

    void Start()
    {
        ConstructStaticCubes();
        ConstructControlledCube();
    }

    private void ConstructStaticCubes()
    {
        int rowsNumber = 5;
        int cubesInRow = 4;
        int[] valuesInRows = new int[] { 2, 1, 1, 0, 0 };
        float stepX = 2.22f;
        float stepZ = 2.27f;

        for (int i = 0; i < rowsNumber; i++)
        {
            for (int j = 0; j < cubesInRow; j++)
            {
                Vector3 spawnPosition = new Vector3(transform.position.x + stepX * j, transform.position.y, transform.position.z - stepZ * i);

                GameObject cubeSpawner = Instantiate(cubeSpawnerPrefab, spawnPosition, Quaternion.identity);
                cubeSpawner.GetComponent<CubeSpawner>().SpawnCube(false, false, false, valuesInRows[i], 0f);
            }
        }
    }

    private void ConstructControlledCube()
    {
        Vector3 spawnPosition = new Vector3(0f, 1f, -13.3f);

        GameObject cubeSpawner = Instantiate(cubeSpawnerPrefab, spawnPosition, Quaternion.identity);
        cubeSpawner.GetComponent<CubeSpawner>().SpawnCube(true, true, false, 0, 0f);
    }
}