using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour
{
    [SerializeField] private GameObject cubePrefab;

    public void SpawnCube(bool isControlled, bool isActive, bool needThrowUp, int value, float delay)
    {
        CheckSpawnCubeValue(value);
        StartCoroutine(SpawnCubeExt(isControlled, isActive, needThrowUp, value, delay));
    }

    IEnumerator SpawnCubeExt(bool isControlled, bool isActive, bool needThrowUp, int value, float delay)
    {
        yield return new WaitForSeconds(delay);

        GameObject spawnedObject = Instantiate(cubePrefab, transform.position, Quaternion.identity);

        spawnedObject.GetComponent<Cube>().SetControlled(isControlled);
        spawnedObject.GetComponent<Cube>().SetActive(isActive);
        spawnedObject.GetComponent<Cube>().SetValue(value);

        if (needThrowUp)
        {
            spawnedObject.GetComponent<Cube>().ThrowCubeUp();
        }

        Destroy(gameObject);
    }

    private void CheckSpawnCubeValue(int value)
    {
        if (value == 10)
        {
            GameplayManager.Instance.Win();
        }
    }
}
