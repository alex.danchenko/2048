using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    public static GameplayManager Instance { get; private set; }

    bool isGameOver;
    bool win;
    int shootsUsed;

    private void Awake()
    {
        Instance = this;
    }

    public bool IsGameOver()
    {
        return isGameOver;
    }

    public void Win()
    {
        isGameOver = true;
        win = true;

        FindObjectOfType<ResultPanel>().DisplayWithResultText("You Win!\n<size=45>Shoots count: " + shootsUsed.ToString() + "</size>");
    }

    public void Lost()
    {
        isGameOver = true;

        FindObjectOfType<ResultPanel>().DisplayWithResultText("You Lose...\n<size=45>Shoots count: " + shootsUsed.ToString() + "</size>");
    }

    public void IncreaseShootsCounter()
    {
        shootsUsed++;
    }
}
