using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextValueGenerator : MonoBehaviour
{
    public static NextValueGenerator Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    public int GetNextValue()
    {
        // as tested it's pretty good algorithm for funny and challenging game but...
        int nextValue = Random.Range(0, 6);

        // ... if a player have some problems with a game we can give him...
        if (FindObjectsOfType<Cube>().Length > 30)
        {
            /* ... 50% chance to get a cube with a value that is equal to nearest cube
             But! It's not so funny and I suggest to test with commented "cheat" part */
            float randomSeed = Random.Range(0f, 1f);

            if (randomSeed > 0.5f)
            {
                nextValue = GetValueOfClosestCube();
            }
        }

        return nextValue;
    }

    private int GetValueOfClosestCube()
    {
        int closestCubeValue = 0;

        float minDistance = Mathf.Infinity;

        Vector3 borderPosition = Border.Instance.transform.position;

        foreach (Cube cube in FindObjectsOfType<Cube>())
        {
            bool isControlledCube = cube.IsControlled();

            if (!isControlledCube)
            {
                float distance = Vector3.Distance(cube.transform.position, borderPosition);

                if (distance < minDistance)
                {
                    closestCubeValue = cube.GetValue();

                    minDistance = distance;
                }
            }
        }

        return closestCubeValue;
    }
}
