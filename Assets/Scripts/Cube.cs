using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class Cube : MonoBehaviour
{
    bool isActive;
    bool isControlled;
    bool isMergeInitiator;
    float xMin = -3.6f;
    float xMax = 3.6f;
    int value;

    [SerializeField] GameObject cubeSpawnerPrefab;
    AsyncOperationHandle<Material> asyncOperationHandle;
    AssetReference cubeMaterialReference;
    Rigidbody rigidBody;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        UpdatePosition();
        ProcessInputs();
    }

    public void SetControlled(bool isControlled)
    {
        this.isControlled = isControlled;

        rigidBody.constraints = isControlled
            ? RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation
            : RigidbodyConstraints.None;
    }

    public bool IsControlled()
    {
        return isControlled;
    }

    public void SetActive(bool isActive)
    {
        this.isActive = isActive;
    }

    public bool IsActive()
    {
        return isActive;
    }

    public void SetValue(int value)
    {
        this.value = value;

        cubeMaterialReference = new AssetReference("Cube" + value + "Mat.mat");

        asyncOperationHandle = cubeMaterialReference.LoadAssetAsync<Material>();

        asyncOperationHandle.Completed += handle =>
        {
            var material = handle.Result;

            GetComponent<MeshRenderer>().material = material;
        };
    }

    public int GetValue()
    {
        return value;
    }

    public bool IsMergeInitiator()
    {
        return isMergeInitiator;
    }

    private void UpdatePosition()
    {
        if (!isControlled)
            return;

        if (GameplayManager.Instance.IsGameOver())
            return;

        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = transform.position.z * -1f;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        float targetX = Mathf.Lerp(transform.position.x, mousePosition.x, 0.5f);
        targetX = targetX < xMin ? xMin : (targetX > xMax ? xMax : targetX);

        transform.position = new Vector3(targetX, transform.position.y, transform.position.z);
    }

    private void ProcessInputs()
    {
        if (!isControlled)
            return;

        if (GameplayManager.Instance.IsGameOver())
            return;

        if (Input.GetMouseButtonDown(0))
        {
            GameObject cubeSpawner = Instantiate(cubeSpawnerPrefab, transform.position, Quaternion.identity);

            int nextCubeValue = NextValueGenerator.Instance.GetNextValue();

            SetControlled(false);
            ShootCube();

            cubeSpawner.GetComponent<CubeSpawner>().SpawnCube(true, true, false, nextCubeValue, 0.1f);
        }
    }

    private void ShootCube()
    {
        Border.Instance.DisableBorderShortly();
        GameplayManager.Instance.IncreaseShootsCounter();

        GetComponent<TrailRenderer>().emitting = true;

        Vector3 forceVector = new Vector3(transform.position.x, transform.position.y, 70f);

        rigidBody.AddForce(forceVector, ForceMode.Impulse);
    }

    public void ThrowCubeUp()
    {
        rigidBody.mass = 10f;

        Vector3 forceVector = new Vector3(Random.Range(-20f, 20f), 80f, transform.position.z + 40f);
        Vector3 torqueVector = new Vector3(Random.Range(-80f, 80f), Random.Range(-80f, 80f), Random.Range(-80f, 80f));

        rigidBody.AddForce(forceVector, ForceMode.Impulse);
        rigidBody.AddTorque(torqueVector, ForceMode.Impulse);

        Invoke("NormalizeMass", 1f);
    }

    private void NormalizeMass()
    {
        if (this != null)
        {
            rigidBody.mass = 2f;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Cube")
        {
            int collidedCubeValue = collision.gameObject.GetComponent<Cube>().GetValue();
            bool collidedCubeIsActive = collision.gameObject.GetComponent<Cube>().IsActive();
            bool collidedCubeIsMergeInitiator = collision.gameObject.GetComponent<Cube>().IsMergeInitiator();

            if (collidedCubeValue == value && (collidedCubeIsActive || isActive) && !collidedCubeIsMergeInitiator && !isMergeInitiator)
            {
                MergeCubes(collision.gameObject);
            }
        }
    }

    private void MergeCubes(GameObject anotherCube)
    {
        GameObject cubeSpawner = Instantiate(cubeSpawnerPrefab, transform.position, Quaternion.identity);
        cubeSpawner.GetComponent<CubeSpawner>().SpawnCube(false, true, true, value + 1, 0f);

        isMergeInitiator = true;

        AudioManager.Instance.PlayMergeSound();

        Destroy(gameObject);
        Destroy(anotherCube);
    }
}
