using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Border : MonoBehaviour
{
    public static Border Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Cube")
        {
            GameplayManager.Instance.Lost();
        }
    }

    public void DisableBorderShortly()
    {
        GetComponent<BoxCollider>().enabled = false;

        CancelInvoke("EnableBorder");
        Invoke("EnableBorder", 0.5f);
    }

    private void EnableBorder()
    {
        GetComponent<BoxCollider>().enabled = true;
    }
}
